<table class=help>
<tr>
	<th>Hilfe zur Einrichtung</th>
	<th>Hilfe zur Konfiguration</th>
	<th>Einwahlnummern</th>
</tr>
<tr>
<td style="width: 300px; text-align: left; vertical-align: top; padding: 15px">
	<li>Zu jeder Durchwahl muss zumindest ein 'SIP Device'(=Anmeldung) eingerichtet werden.
	<li>Legen Sie zuerst eine Durchwahl an, klicken Sie dann auf 'SIP-Deivces'>'Show'
	<li>Jeder Apperat, Softwareclient oder Gateway braucht seinen eigenen Eintrag unter 'SIP-Devices'. Mehrere
	Clients k&ouml;nnen nicht eine einzige Anmeldung gleichzeitig benutzen.
</td>
<td style="width: 300px; text-align: left; vertical-align: top; padding: 15px">
	<li>SIP-Server: voip.funkfeuer.at
	<li>Port: 5060
	<li>User-ID: (siehe 'SIP-Devices')
	<li>Passwort: (siehe 'SIP-Devices')
	<br><br><b>Optionale Parameter:</b>
	<li>Preffered Codec: G711a
	<li>Register: YES
	<li>NAT: YES
	<li>STUN-Server: stun.funkfeuer.at
</td>
<td style="width: 300px; text-align: left; vertical-align: top; padding: 15px">
	<li>Intern: <b>1xxx</b>
	<li>Extern: <br> &nbsp; <b>01/2360933/xxxx</b><br> &nbsp; <b>0720/ 550933/xxxx</b>
	<li>optional ENUM: <br> &nbsp; <b>0780/700888/xxx</b>
	<!--<li>Festnetz/Mobil: <b>01/302555/xxx</b> (exprimentell) -->
</td>
</tr>
</table>
<div style="padding: 15px; margin: 20px; border: 1px solid red; background-color: #ffcccc; font-weight: bold; display: block; width: 600px;">
Das VOIP Service ist nicht f&uuml;r Notrufe oder andere lebenswichtige Services geeignet.
<p>
Jeder Benutzer muss dieser Einschr&auml;nkung zustimmen, oder darf andernfalls das Service nicht nutzen.
</div>

