<table class=help id="helpTable6" style="display: none;">
<tr>
	<th>Erklärung zum Formular IPv6-Device</th>
</tr>
<tr>
<td style="width:450px;">
	<h3>Zuordnung</h3>
	Legt fest, ob diese IPv6-Adresse mit einem v4-Device verknüpft ist, oder "nur" mit dem aktuellen Node.<br>
	<u>- Device:</u> Name vom vorhandenen Device wird automatisch übernommen (Anwendungsfall: gleiches OLSR-Interface oder gleiches Gerät hat v4 und v6 Adresse)<br>
	<u>- Node:</u> Name kann frei gewählt werden (Anwendungsfall: Gerät oder Interface ist v6-only bzw. hat keinen Bezug zu einer vorhandenen IPv4-Adresse)
	<h3>Name</h3>
	Legt den Hostnamen fest. Dieser Name wird für Smokeping (<em>devicename.{$node_name}</em>) und DNS (<em>devicename.{$node_name}</em>.wien.funkfeuer.at) verwendet
	<h3>IPv6</h3>
	Legt die zum Node/Device-Namen zugeordnete IPv6-Adresse fest.<br>
	Bei <b>Neuanlage</b> muss diese IPv6-Adresse eingegeben werden! Es gibt keine Auswahlmöglichkeit von freien IPv6-Adressen. Dennoch besteht eine Eingabehilfe 
	mit einem Click auf die zwei möglichen IPv6-Notationen:<br>
	<u>- EUI64</u>: fügt eine Adresse in EUI64-Notierung ein. Der hintere Teil muss hier noch korrigiert werden und ist in der Regel von der MAC-Adresse/Seriennummer des Routers abhängig.</b><br>
	<u>- UserBlock/NodeID:</u> fügt eine Adresse mit enthaltener Node-ID ein. Der hintere Teil muss hier gegebenenfalls noch angepasst werden.<br>
	<br>
	Bei nachfolgenden <b>Änderungen</b> kann die IPv6 nicht mehr geändert werden und bleibt in der Anzeige readonly. Muss die IP selbst 
	geändert werden, ist ein neues Device anzulegen und ggf. das vorhandene zu löschen.

	<h3>Antenne, Hardware, SSID, MAC</h3>
	Optionale Felder für Detail-Information

	<h3>Smokeping</h3>
	Aktiviert den Smokeping zu dieser IPv6-Adresse. Monitoring dazu findet sich unter <a href='http://smokeping.funkfeuer.at/smokeping/olsr2/?target={$node_name}' target=_new>smokeping.funkfeuer.at/smokeping/olsr2/?target={$node_name}</a>.<br>
	Im Smokeping-Monitoring werden Hosts automatisch entfernt, wenn sie mehr als 360 Tage nicht mehr pingbar waren. Neue und jüngst geänderte Devices bleiben nur 10 Tage ohne Ping in der Liste.
	<h3>AAAA</h3>
	Aktiviert für den Hostnamen <em>devicename.nodename</em>.wien.funkfeuer.at die "forward DNS resolution" durch einen AAAA-Record. Dadruch kann der Hostname in die IPv6-Adresse umgeschl&uuml;sselt werden.<br>
	Für jeden Hostnamen darf nur <b>eine</b> IPv6-Adresse für AAAA aktiviert sein.
	<h3>PTR</h3>
	Aktiviert für die IPv6-Adresse die "reverse DNS resolution" durch einen PTR-Record. Dadruch kann zu dieser IPv6-Adresse der Hostname gefunden werden.<br>
	<br>Es können mehrere unterschiedliche IPv6-Adressen mit dem selben Namen angelegt werden, doch nur eine davon kann einen AAAA-record haben.
	<br>&nbsp;
</td>
</tr>
</table>
<script>
function toggleTable(a) {ldelim}
	var all = [ "6" ];
	for (i in all) {ldelim}
		var x = document.getElementById("helpTable"+all[i]);
		var b = document.getElementById("toggleButton"+all[i]);
		if ((a==all[i]) && (x.style.display === "none")) {ldelim}
			x.style.display = "block";
			b.innerHTML ="Hide Help";
		{rdelim} else {ldelim}
			x.style.display = "none";
			b.innerHTML ="Show Help";
		{rdelim}
	{rdelim}
{rdelim}
</script>
