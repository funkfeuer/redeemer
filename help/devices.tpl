<table class=help id="helpTable4" style="display: none;">
<tr>
	<th>Erklärung zu Devices</th>
</tr>
<tr>
<td style="width:450px;">
	<h3>Felder</h3>
	<li>Muss: Name, Smokeping</li>
	<li>Automatisch: IP-Adresse*</li>
	<li>Optional: SSID, Antenne, Hardware, MAC</li>
	<br>* nächste freie IP-Adresse wird beim Speichern ermittelt
	<h4>Aktionen</h4>
	<li>DEL=Löschen: IP freigeben</li>
	<li>MOVE=Verschieben: zu anderen Node verlegen</li>

	<!--br>**eine eventuell zugeordnete IPv6-Adresse bleibt am aktuellen Node bestehen-->
	<br>&nbsp;
</td>
</tr>
</table>

<table class=help id="helpTable6" style="display: none;">
<tr>
	<th>Erklärung zu IPv6-Adressen</th>
	<th>Erklärung zu v642</th>
</tr>
<tr>
<td>
	<h3>Felder</h3>
	<li>Name*</li>
	<li>Zuordnung: Node oder Device**</li>
	<li>MAC: optional</li>
	<li>Smokeping</li>
	<li>AAAA: Hostname zu IPv6 auflösen*</li>
	<li>PTR: Hostname von IPv6 finden*</li>
	<br>*Es können mehrere unterschiedliche  IPv6-Adressen mit dem selben Namen angelegt werden, doch nur eine davon kann einen AAAA-record haben.
	<br>
	<br>**Zuordnung zu einem Device: Name vom vorhandenen Device wird automatisch übernommen (Anwendungsfall: gleiches OLSR-Interface oder gleiches Gerät 
	hat v4 und v6 Adresse)
	<br>Zuordnung zum Node: Name kann frei gewählt werden (Anwendungsfall: Gerät oder Interface ist v6-only bzw. hat keinen Bezug zu einer vorhandenen IPv4-Adresse)
	<br>&nbsp;
</td>
<td>
	<h3>Felder</h3>
	<li>Muss: Name</li>
	<li>Auswahlmöglichkeit: IP</li>
	<br>Pro Node ist nur 1 v642-Adresse vorgesehen. Das Setup besteht aus einem Triplett:<br><br>
	<li>185.194.20.0/20 für deinen Rourter</li>
	<li>2a02:61:0:ee:1:: für deinen Router (destination)</li>
	<li>2a02:61:0:ee:0:: für zentralen Wrap-Server (source)</li>
	<br>
	<br><a target=wiki href="https://wiki.funkfeuer.at/wiki/Projekte/v642/IPv4_Support">Details im Wiki</a>
	<br>&nbsp;
</td></tr>
</table>
<script>
function toggleTable(a) {ldelim}
	var all = [ "4" , "6" ];
	for (i in all) {ldelim}
		var x = document.getElementById("helpTable"+all[i]);
		var b = document.getElementById("toggleButton"+all[i]);
		if ((a==all[i]) && (x.style.display === "none")) {ldelim}
			x.style.display = "block";
			b.innerHTML ="Hide Help";
		{rdelim} else {ldelim}
			x.style.display = "none";
			b.innerHTML ="Show Help";
		{rdelim}
	{rdelim}
{rdelim}
</script>