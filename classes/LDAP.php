<?php

class LDAP {

    //GENERAL LDAP FUNCTIONS

    function connect($server, $port) {

	$connection = ldap_connect($server, $port);
	ldap_set_option($connection, LDAP_OPT_PROTOCOL_VERSION, 3);
	ldap_set_option($connection, LDAP_OPT_REFERRALS, 0);

	return $connection;
    }

    function bind($connection, $basedn, $basepass) {

	return ldap_bind($connection, $basedn, $basepass);
    }

    function search($connection, $searchdn, $filter, $attributes = array()) {

	if (!empty($attributes)) {
	    $sr = ldap_search($connection, $searchdn, $filter, $attributes);
	} else {
	    $sr = ldap_search($connection, $searchdn, $filter);
	}
	if ($sr) {
	    return ldap_get_entries($connection, $sr);
	} else {
	    return false;
	}
    }

    function addRecord($connection, $adddn, $record) {

	return ldap_add($connection, $adddn, $record);
    }

    function modifyRecord($connection, $modifydn, $record) {

	return ldap_modify($connection, $modifydn, $record);
    }

    function deleteRecord($connection, $dn) {

	return ldap_delete($connection, $dn);
    }

    function close($connection) {

	return ldap_close($connection);
    }

    // USER FUNCTIONS (FUNKFEUER LDAP SPECIFIC)

    function add_member($userdata = array()) {
	$connection = $this->connect(LDAP_SERVER, LDAP_PORT); //connect to ldap server
	$this->bind($connection, LDAP_USER_DN, LDAP_PASS); //bind with service account credentials
	$dn = 'uid=' . $userdata['uid'] . ',ou=users,dc=funkfeuer,dc=at'; //create user with data from input
	return ($this->addRecord($connection, $dn, $userdata)); //create attributes with data from input
    }

    function update_member($id, $userdata = array()) {
	$filter = "(&(objectClass=posixAccount)(uidnumber=" . $id . "))"; //filter for user search
	$connection = $this->connect(LDAP_SERVER, LDAP_PORT); //connect to ldap server
	$this->bind($connection, LDAP_USER_DN, LDAP_PASS); //bind with service account credentials
	$search = $this->search($connection, LDAP_BASE_DN, $filter); //search for user
	if (!$search) {
	    return false; //user not found
	}
	if ($search['count'] != 1) {
	    return false; //more than one user found! abort!
	}
	$dn = $search[0]['dn'];

	return ($this->modifyRecord($connection, $dn, $userdata)); //update user
    }

    function change_password($id, $password) {
	$filter = "(&(objectClass=posixAccount)(uidNumber=" . $id . "))"; //filter for user search
	$connection = $this->connect(LDAP_SERVER, LDAP_PORT); //connect to ldap server
	$this->bind($connection, LDAP_USER_DN, LDAP_PASS); //bind with service account credentials
	$search = $this->search($connection, LDAP_BASE_DN, $filter); //search for user
	if (!$search) {
	    return false; //user not found
	}
	if ($search['count'] != 1) {
	    return false; //more than one user found! abort!
	}
	$userdata['dn'] = $search[0]['dn'];

	$record = array();
	$record['userPassword'] = $password;
	return $this->modifyRecord($connection, $userdata['dn'], $record); //change the password
    }

    function user_login($username, $password) {
	$filter = "(&(objectClass=posixAccount)(uid=" . ldap_escape($username, "", LDAP_ESCAPE_FILTER) . "))"; //filter for user search
	$connection = $this->connect(LDAP_SERVER, LDAP_PORT); //connect to ldap server
	$this->bind($connection, LDAP_USER_DN, LDAP_PASS); //bind with service account credentials
	$search = $this->search($connection, LDAP_BASE_DN, $filter); //search for user
	if (!$search) {
	    return false; //user not found
	}
	if ($search['count'] != 1) {
	    return false; //more than one user found! abort!
	}
	$userdata['dn'] = $search[0]['dn'];
	$userdata['uid'] = $search[0]['uid'][0];
	$userdata['uidnumber'] = $search[0]['uidnumber'][0];
	$userdata['cn'] = $search[0]['cn'][0];
	$userdata['sn'] = $search[0]['sn'][0];
	$userdata['givenname'] = $search[0]['givenname'][0];
	$userdata['mail'] = $search[0]['mail'][0];
	$userdata['memberof'] = $search[0]['memberof']; // --> can have multiple entries!!
	$userdata['userpassword'] = $search[0]['userpassword'][0];

	if ($this->bind($connection, $userdata['dn'], $password)) {
		return $userdata;
	} else {
		return false;
	}
    }

    function delete_member($id) {
	$filter = "(&(objectClass=posixAccount)(uidNumber=" . ldap_escape($id, "", LDAP_ESCAPE_FILTER) . "))"; //filter for user search
	$connection = $this->connect(LDAP_SERVER, LDAP_PORT); //connect to ldap server
	$this->bind($connection, LDAP_USER_DN, LDAP_PASS); //bind with service account credentials
	$search = $this->search($connection, LDAP_BASE_DN, $filter); //search for user
	if (!$search) {
	    return false;
	}
	if ($search['count'] != 1) {
	    return false;
	}
	$userdata['dn'] = $search[0]['dn'];
	return $this->deleteRecord($connection, $userdata['dn']);
    }

}

?>
