Hi,

für Dich wurde ein Benutzer in unserer Datenbank angelegt.

Die Daten dafür lauten:
Nickname: {$nickname}
Passwort: {$password}

Das Frontend erreichst Du unter: https://portal.funkfeuer.at/wien/

Mit den besten Wünschen,
Dein FunkFeuer Team
