{if $error eq ""}
	{if $do eq "true" or $action eq "delete" or $action eq "resign" or $action eq "accept" or $action eq "decline" }
		<div class="formSuccess">Abfragen erfolgreich durchgef&uuml;hrt.</div>
		{if $section eq "devices"}
			<script>window.location='{$PHP_SELF}?section=devices&id_nodes={$id_nodes}';</script>
		{elseif $section eq "ip6"}
			<script>window.location='{$PHP_SELF}?section=devices&id_nodes={$id_nodes}';</script>
		{elseif $section eq "v642"}
			<script>window.location='{$PHP_SELF}?section=devices&id_nodes={$id_nodes}';</script>
		{elseif $section eq "voip_sip"}
			<script>window.location='{$PHP_SELF}?section=voip&id_members={$id_members}';</script>
		{elseif $section eq "member" and $action eq "delete"}
			<script>window.location='{$PHP_SELF}?section=member&action=search';</script>
		{elseif $section eq "member"}
			<script>window.location='{$PHP_SELF}';</script>
		{else}
			<script>window.location='{$PHP_SELF}?section={$section}&id_members={$id_members}';</script>
		{/if}
	{/if}
{else}
	<div class="formFailure">
		<strong>Fehler:</strong>
		<pre>{$error}</pre>
		{if $error_detail ne ""}
			<br/>
			<strong>Details:</strong>
			<pre>{$error_detail}</pre>
		{/if}
	</div>
{/if}
