<?php
//ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);

ini_set('include_path', '.:lib:classes');
ini_set('session.cookie_samesite', 'Strict');
ini_set("session.cookie_secure", 1);
include_once('VerifyMailbox.php');
include_once('config.php');
include_once('Credentials.php');
include_once('Misc.php');
include_once('DB.php');
include_once('DatabaseUtils.php');

set_time_limit(90);
ini_set('max_execution_time', 90);

if (!isset($options)) $options=array();

$debug_verify=false;
$id="";
if (php_sapi_name() == 'cli') {
	$approved=true;
	if (isset($argv[1])) $email=$argv[1]; else { echo "no email address specified\nusage: php -e verifymail.php <email> <id> <debug> <forcesmtp>\n"; exit; }
	if (isset($argv[2])) $id=$argv[2];
	if ((isset($argv[3])) && ($argv[3]==1)) $debug_verify=true;
	if ((isset($argv[4])) && ($argv[4]==1)) $force_smtp=true; else $force_smtp=false;
} else {
	// die on unsave charachters in request-URI to avoid XSS
	if (preg_match("/[\"\'<>#%{}|\^~\[\]`]/", $_SERVER['QUERY_STRING'])) {
		header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad Request ', true, 400);
		exit;
	}
	session_start();
	if (preg_match('/\/'.BASE.'\//', $_SESSION['uri']) == 0) {
		session_destroy();
		session_start();
	}
	restoreCredentials();

	if (($credentials->getId()) && (isset($_POST['m'])) && ($_POST['m']!=="")) {
		$approved=true;
		if ((isset($_POST['i'])) && ($_POST['i']!=="")) $id=$_POST['i'];
		$email = urldecode($_POST['m']);
	} else if (($credentials->getId()) && (isset($_GET['m'])) && ($_GET['m']!=="")) {
		$approved=true;
		if ((isset($_GET['i'])) && ($_GET['i']!=="")) $id=$_GET['i'];
		$email = urldecode($_GET['m']);
	}
	if ((isset($_GET['f'])) && ($_GET['f']=="1")) $force_smtp=true; else $force_smtp=false;
}

if ($approved) {
	$mail = new VerifyEmail();

	//to avoid blacklisting try to cache some results and block retries for 15mins
	if ((isset($_SESSION['smtp_wait'])) && (isset($_SESSION['smtp_wait'][$email]))) {
		if ($_SESSION['smtp_wait'][$email] > time()) {
			$remain=$_SESSION['smtp_wait'][$email] - time();
			header('Content-Type: application/json');
			$result=array('code'=>'498','line'=>'Greylist-Wartezeit: '.$remain."sek", 'formatvalidation'=>verifyEmail::validate($email), 'verbose'=>'respecting greylist timeout: '.$remain."sec");
			$output=array('mail'=>$email, 'code'=>$result['code'], 'formatvalidation'=>verifyEmail::validate($email), 'verbose'=>$result['line'],'id'=>$id,'ca'=>$result['ca']);
			print json_encode($output, true);
			exit;
		}
	}

	if ((isset($_SESSION['cached'])) && (isset($_SESSION['cached'][$email]))) {
		if ($_SESSION['cached'][$email]['time'] > time()) {
			$remain=$_SESSION['cached'][$email]['time'] - time();
			header('Content-Type: application/json');
			$result=$_SESSION['cached'][$email]['result'];
			$output=array('mail'=>$email, 'code'=>$result['code'], 'formatvalidation'=>verifyEmail::validate($email), 'verbose'=>($result['code']=='001' ? '' : 'Cached: ').$result['line'],'id'=>$id,'ca'=>$result['ca']);
			print json_encode($output, true);
			exit;
		}
	}

	$databaseUtils = new DatabaseUtils();
	$v_row=$databaseUtils->getFirstRow("select h.hash, to_char(h.last_checked,'dd.mm.yyyy') as last_checked, h.text, h.ca, h.code, extract(epoch from date_trunc('seconds', now())-last_checked) as age from verify h where hash='".md5($email)."'");
	if (count($v_row)>0) $is_new=false; else $is_new=true;

	if ((!$is_new) && (!$force_smtp) && ($v_row['age'] < SMTP_DAYS * 86400)) { //use old result 
		if (php_sapi_name() !== 'cli') {
			/*cache result for 5min*/
			if (!isset($_SESSION['cached'])) $_SESSION['cached']=array();
			$result=array('code'=>$v_row['code'], 'line'=>$v_row['text'],'formatvalidation'=>verifyEmail::validate($email),'verbose'=>$v_row['text'],'ca'=>$v_row['ca']);
			$_SESSION['cached'][$email]=array('time'=>time() +5*60, 'result'=>$result);
			header('Content-Type: application/json');
		}
		$output=array('mail'=>$email, 'code'=>$v_row['code'], 'formatvalidation'=>verifyEmail::validate($email), 'verbose'=>''.$v_row['last_checked'].': '.$v_row['text'],'id'=>$id,'ca'=>$v_row['ca']);
		print json_encode($output, true);
		exit;	
	}

	if (php_sapi_name() != 'cli') {
		// flag this mail is being checked just right now
		if (!isset($_SESSION['cached'])) $_SESSION['cached']=array();
		$_SESSION['cached'][$email]=array('time'=>time() +92, 'result'=>array('code'=>'001','line'=>'Prüfung läuft...', 'formatvalidation'=>true, 'verbose'=>'smtp connection already opened, recheck in a few seconds'));

		//release session lock
		session_write_close();
	}

	$mail->setStreamTimeoutWait(25);
	$mail->setEmailFrom(MEMBER_SUBSCRIBE_MAIL_FROM);
	if ((php_sapi_name() == 'cli') && ($debug_verify)) {
		$mail->Debug= TRUE;
		$mail->Debugoutput= 'echo';
	} else {
		header('Content-Type: application/json');
	}

	// Email to check
	$format=verifyEmail::validate($email);

	if ($format) {
		// Check if email is valid and exist
		$result=$mail->check($email,$options);
		if (!$result) {
			$result=array('code'=>'995','line'=>'SMTP-Server lieferte Fehlermeldung', 'formatvalidation'=>true, 'verbose'=>'smtp returned an error');
		} else {
			if (preg_match('/mailbox unavailable|user unknown|unkn?own user|mailbox not|does not exist|no mailbox by that name|Address invalid|Nosuchuser|User doesn\'t exist|address unknown|no such recipient|mailbox '.$email.' unknown|no mailbox here by that name/iD',$result['line'])) {
				$result['code']='499';
			}
		}
	} else {
		$result=array('code'=>'000','line'=>'falsches E-Mail Format', 'formatvalidation'=>false, 'verbose'=>'enter a valid email address');
	}

	if (php_sapi_name() != 'cli') {
		//re optain the session and save the results
		session_start();
	}

	if ((in_array($result['code'], array('450','451','452','454'))) || (preg_match('/greylist/iD',$result['line']))) {
		// set timeout for next verification
		if (!isset($_SESSION['smtp_wait'])) $_SESSION['smtp_wait']=array();
		$_SESSION['smtp_wait'][$email]=time() +15*60;
	} else {
    	//save result to database
		$text=str_replace('\u003C','<',str_replace('\u003E','>',$result['line']));
		$text=str_replace($email,'',$text);
		if ($is_new) $databaseUtils->sendQuery("INSERT into verify values ('".md5($email)."', date_trunc('seconds', now()), ".$result['code']."+0, ".$result['ca']."+0, '".$text."')");
		else $databaseUtils->sendQuery("UPDATE verify set last_checked=date_trunc('seconds', now()), code=".$result['code']."+0, ca=".$result['ca']."+0, text='".$text."' WHERE hash='".md5($email)."'");
	}

	/*cache result for 5min*/
	if (!isset($_SESSION['cached'])) $_SESSION['cached']=array();
	$_SESSION['cached'][$email]=array('time'=>time() +5*60, 'result'=>$result);


	$output=array('mail'=>$email, 'code'=>$result['code'], 'formatvalidation'=>$format, 'verbose'=>$result['line'],'id'=>$id, 'ca'=>$result['ca']);
	print str_replace('\u003C','<',str_replace('\u003E','>',json_encode($output, true)))."\n";
} else {
	header('Content-Type: application/json');
	$output=array('error'=>'login required');
	print json_encode($output, true);
	exit;
}

?>
